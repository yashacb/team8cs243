<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Department : {{$dept}}</title>

    <link rel="stylesheet" href="{{URL::asset('bootstrap/dist/css/bootstrap.min.css')}}">
    <link href="{{URL::asset('css/dashboard.css')}}" rel="stylesheet">
    <link href="{{URL::asset('bootstrap/dist/css/style.min.css')}}" rel="stylesheet">
    <style>
    	#collapse2
    	{
    		max-height: 60em;
    		overflow-y:scroll;
    	}
    </style>
</head>
<body>
    <div id="wrapper">
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="navbar-header">

                <a class="navbar-brand" href="{{route('dashboard')}}">Student Information System</a>
      </div>
      <form method = "POST" action = {{route('search')}}>
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id = "token" >
      <div class="navbar-left">
          <div class="input-group custom-search-form">
              <input type="text" class="form-control" placeholder="Search..." name = "text" required>
              <span class="input-group-btn">
                <button class="btn btn-default"  type="submit" >
                  <i class= "	glyphicon glyphicon-search" ></i>
                </button>
              </span>

          </div>
      </div>
    </form>
      <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> {{$user->name}} <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><span class="glyphicon glyphicon-envelope"></span> Change Password</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{ route('logout') }}"><span class="glyphicon glyphicon-off"></span> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
          </nav>
      <div class="row">
        <div class="col-sm-3">
            <!-- Left column -->
            <a href="{{route('dashboard')}}"><strong><i class="glyphicon glyphicon-home"></i> Dashboard</strong></a>

            <hr>

            <ul class="nav nav-stacked">
                <li class="nav-header "> <a href="#" data-toggle="collapse" data-target="#userMenu">Departments </a>
                    <ul class="nav nav-stacked collapse" id="userMenu">
                        <li> <a data-toggle="collapse" data-target="#year_cse" href="#" >&nbsp; Mechanical Engineering &nbsp;</a>
                        </li>
                        <li><a href="#" data-toggle="collapse" data-target="#year_ece">&nbsp; Electronics and Communication Engineering </a>
                        </li>
                        <li><a href="#" data-toggle="collapse" data-target="#year_eee">&nbsp; Electrical Engineering</a>
                        </li>
                        <li><a href="#" data-toggle="collapse" data-target="#year_mnc">&nbsp; Department of Mathematics</a>
                        </li>
                        <li><a href="#" data-toggle="collapse" data-target="#year_me">&nbsp; Computer Science and Engineering</a>
                        </li>
                        <li><a href="#" data-toggle="collapse" data-target="#year_ce">&nbsp; Civil Engineering</a>
                          </li>
                        <li><a href="#" data-toggle="collapse" data-target="#year_cst">&nbsp; Chemical Science and Technology</a>
                        </li>
                        <li><a href="#" data-toggle="collapse" data-target="#year_c">&nbsp; Department of Chemistry</a>
                        </li>
                    </ul>
                </li>
                <li class="nav-header "> <a href="#" data-toggle="collapse" data-target="#menu2"> Courses </a>

                  <ul class="nav nav-stacked collapse" id="menu2">
                    @for($i = 0 ; $i < count($courses) ; $i++)
                    <li> <a href="{{route('course' , ['course_num' => $courses[$i]])}}">&nbsp; {{$courses[$i]}}</a></li>
                    @endfor
                  </ul>
                </li>

            </ul>

        </div>
        <div class="col-sm-10">

        <div class="row">
        	<div class="col-md-12">
      			<div class="box box-solid box-primary">
        			<div class="box-header">
          				<h3 class="box-title">Mechanical Engineering</h3>
        			</div><!-- /.box-header -->
        			<div class="box-body">
          				<p><h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The department of mechanical engineering, being one of the largest and oldest departments of the institute, caters to its students with class tutorial and state-of-the-art laboratories. The department is continuously striving to achieve excellence in education, academic and industry oriented research as well as consultancy work with service to the society.
We aim to provide our students with a perfect blend of intellectual and practical experiences, that helps them to serve our society and address a variety of needs. At the end of our program, students are prepared for entry-level work as a mechanical engineer as well as for the post-graduate study in mechanical engineering or in another discipline, where a fundamental engineering background constitutes a desirable foundation. Academic course work and projects are designed to endow students with the ability to apply knowledge of science, mathematics, and engineering, and the capability to work effectively in multidisciplinary teams, providing leadership and technical expertise.</h4></p>
        			</div><!-- /.box-body -->
      			</div><!-- /.box -->
    		</div>
    	</div>



    	<div class="panel-group" id="accordion">
  <div class="panel panel-primary">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
        Courses</a>
      </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse in">
      <div class="panel-body">

     	<div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab">Semester 1</a></li>
                  <li><a href="#tab_2" data-toggle="tab">Semester 2</a></li>
                  <li><a href="#tab_3" data-toggle="tab">Semester 3</a></li>
                  <li><a href="#tab_4" data-toggle="tab">Semester 4</a></li>
                  <li><a href="#tab_5" data-toggle="tab">Semester 5</a></li>
                  <li><a href="#tab_6" data-toggle="tab">Semester 6</a></li>
                  <li><a href="#tab_7" data-toggle="tab">Semester 7</a></li>
                  <li><a href="#tab_8" data-toggle="tab">Semester 8</a></li>


                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">

                  	<div class="box">
                <div class="box-header">
                  <h3 class="box-title">Semester 1</h3>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <table class="table table-striped">
                    <tr>
                      <th style="width: 20%">Course No.</th>
                      <th style="width:40%">Course Name</th>
                      <th style="width:10%">L</th>
                      <th style="width: 10%">T</th>
                      <th style="width:10%">P</th>
                      <th style="width:10%">C</th>
                    </tr>
                      @for($i = 0 ; $i < count($sems[1]) ; $i++)
                      <tr>
                        <td>{{$sems[1][$i]->course_num}}</td>
                        <td><a href="{{route('course' , ['course_num'=>$sems[1][$i]->course_num])}}">{{$sems[1][$i]->course_name}}</a></td>
                        <td><span class="badge bg-red">{{$sems[1][$i]->l}}</span></td>
                        <td><span class="badge bg-green">{{$sems[1][$i]->t}}</span></td>
                        <td><span class="badge bg-yellow">{{$sems[1][$i]->p}}</span></td>
                        <td><span class="badge bg-light-blue">{{$sems[1][$i]->c}}</span></td>
                      </tr>
                    @endfor
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              </div><!-- /.tab-pane -->
                @for($i = 2 ; $i <= 8 ; $i++)
                  <div class="tab-pane" id="tab_{{$i}}">
                    <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Semester {{$i}}</h3>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <table class="table table-striped">
                    <tr>
                      <th style="width: 20%">Course No.</th>
                      <th style="width:40%">Course Name</th>
                      <th style="width:10%">L</th>
                      <th style="width: 10%">T</th>
                      <th style="width:10%">P</th>
                      <th style="width:10%">C</th>
                    </tr>
                    @for($j = 0 ; $j < count($sems[$i]) ; $j++)
                    <tr>
                      <td>{{$sems[$i][$j]->course_num}}</td>
                      <td><a href="{{route('course' , ['course_num'=>$sems[$i][$j]->course_num])}}">{{$sems[$i][$j]->course_name}}</a></td>
                      <td><span class="badge bg-red">{{$sems[$i][$j]->l}}</span></td>
                      <td><span class="badge bg-green">{{$sems[$i][$j]->t}}</span></td>
                      <td><span class="badge bg-yellow">{{$sems[$i][$j]->p}}</span></td>
                      <td><span class="badge bg-light-blue">{{$sems[$i][$j]->c}}</span></td>
                    </tr>
                    @endfor
                    </tr>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
                  </div><!-- /.tab-pane -->
                  @endfor

                </div><!-- /.tab-content -->
              </div><!-- nav-tabs-custom -->


    </div>
  </div>
  </div>
  <?php $tabs = 9 ; ?>
  <div class="panel panel-primary">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
        Students</a>
      </h4>
    </div>
    <div id="collapse2" class="panel-collapse collapse">
      <div class="panel-body">

        <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_9" data-toggle="tab">2013</a></li>
                  <?php $tabs++ ; ?>
                  @for($i = $base + 1 ; $i <= $last ; $i++)
                  <li><a href="#tab_{{$tabs}}" data-toggle="tab">{{$i}}</a></li>
                  <?php $tabs++ ?>
                  @endfor
                  <?php $tabs = 9 ?>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_{{$tabs}}">
                    <?php $tabs++ ; ?>
                    <div class="row">

              @for($i = 0 ; $i < count($data[$base]) ; $i++)

             <div class="col-xs-12 col-sm-12 col-md-5 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 toppad" >


          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">{{$data[$base][$i]->name}}</h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="{{URL::asset('images/User.png')}}" class="img-circle img-responsive"> </div>
                <div class=" col-md-9 col-lg-9 ">
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td>Department:</td>
                        <td>{{$data[$base][$i]->dept}}</td>
                      </tr>
                      <tr>
                        <td>Roll Number:</td>
                        <td>{{$data[$base][$i]->roll_no}}</td>
                      </tr>
                      <tr>
                        <td>Year</td>
                        <td>{{$data[$base][$i]->year}}</td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td><a href="{mailto:{$data[$i]->email}}">{{$data[$base][$i]->email}}</a></td>
                      </tr>
                      <tr>
                        <td>Phone Number</td>
                        <td>{{$data[$base][$i]->contact}}
                        </td>

                      </tr>

                    </tbody>
                  </table>
                  <?php echo '<hr>' ; ?>

                  <a href="{{route('viewst' , ['roll_no' => $data[$base][$i]->roll_no])}}" class="btn btn-primary">View More</a>&nbsp;&nbsp;&nbsp;
                  <a href="#" class="btn btn-primary">Generate PDF</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endfor
        </div>
                </div><!-- /.tab-pane -->
                @for($j = $base + 1 ; $j <= $last ; $j++)
                  <div class="tab-pane" id="tab_{{$tabs}}">
                    <?php $tabs++ ; ?>
                    <div class="row">

              @for($i = 0 ; $i < count($data[$base]) ; $i++)
             <div class="col-xs-12 col-sm-12 col-md-5 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 toppad" >


          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">{{$data[$j][$i]->name}}</h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="{{URL::asset('images/User.png')}}" class="img-circle img-responsive"> </div>
                <div class=" col-md-9 col-lg-9 ">
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td>Department:</td>
                        <td>{{$data[$j][$i]->dept}}</td>
                      </tr>
                      <tr>
                        <td>Roll Number:</td>
                        <td>{{$data[$j][$i]->roll_no}}</td>
                      </tr>
                      <tr>
                        <td>Year</td>
                        <td>{{$data[$j][$i]->year}}</td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td><a href="mailto:{{$data[$j][$i]->email}}">{{$data[$base][$i]->email}}</a></td>
                      </tr>
                      <tr>
                        <td>Phone Number</td>
                        <td>{{$data[$j][$i]->contact}}
                        </td>

                      </tr>

                    </tbody>
                  </table>


                  <a href="{{route('viewst' , ['roll_no' => $data[$base][$i]->roll_no])}}" class="btn btn-primary">View More</a>&nbsp;&nbsp;&nbsp;
                  <a href="#" class="btn btn-primary">Generate PDF</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endfor
        </div>
                </div><!-- /.tab-pane -->
                @endfor
                </div><!-- /.tab-content -->
              </div><!-- nav-tabs-custom -->


      </div>
    </div>
  </div>






              </div>
        </div>
      </div>
</body>
<script src="{{URL::asset('bootstrap/js/tests/vendor/jquery.min.js')}}"></script>
<script src="{{URL::asset('bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script>

      $(document).ready(function(){$(".alert").addClass("in").fadeOut(4500);

        /* swap open/close side menu icons */
      $('[data-toggle=collapse]').click(function(){
        // toggle icon
        $(this).find("i").toggleClass("glyphicon-chevron-right glyphicon-chevron-down");
      });
    });
    </script>
</html>
