<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard</title>

   <link rel="stylesheet" href="{{URL::asset('css/dashboard.css')}}">
<link rel="stylesheet" href="{{URL::asset('bootstrap/dist/css/bootstrap.min.css')}}">
</head>
<body>
    <div id="wrapper">
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="navbar-header">

                <a class="navbar-brand" href="{{route('dashboard')}}">Student Information System</a>
      </div>
      <form method = "POST" action = {{route('search')}}>
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id = "token" >
      <div class="navbar-left">
          <div class="input-group custom-search-form">
              <input type="text" class="form-control" placeholder="Search..." name = "text" required>
              <span class="input-group-btn">
                <button class="btn btn-default"  type="submit" >
                  <i class= "	glyphicon glyphicon-search" ></i>
                </button>
              </span>

          </div>
      </div>
    </form>
      <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> {{$user->name}} <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><span class="glyphicon glyphicon-envelope"></span> Change Password</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{ route('logout') }}"><span class="glyphicon glyphicon-off"></span> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
          </nav>
      <div class="row">
        <div class="col-sm-3">
            <!-- Left column -->
            <a href="{{route('dashboard')}}"><strong><i class="glyphicon glyphicon-home"></i> Dashboard</strong></a>

            <hr>

            <ul class="nav nav-stacked">
                <li class="nav-header "> <a href="#" data-toggle="collapse" data-target="#userMenu">Departments </a>
                    <ul class="nav nav-stacked collapse" id="userMenu">
                        <li> <a href="{{route('departments' , ['dept' => 'mech'])}}" >&nbsp; Mechanical Engineering &nbsp;</a>
                        </li>
                        <li><a href="{{route('departments' , ['dept' => 'mech'])}}" >&nbsp; Electronics and Communication Engineering </a>
                        </li>
                        <li><a href="{{route('departments' , ['dept' => 'mech'])}}" data-toggle="collapse" data-target="#year_eee">&nbsp; Electrical Engineering</a>
                        </li>
                        <li><a href="{{route('departments' , ['dept' => 'mech'])}}" data-toggle="collapse" data-target="#year_mnc">&nbsp; Department of Mathematics</a>
                        </li>
                        <li><a href="{{route('departments' , ['dept' => 'mech'])}}" data-toggle="collapse" data-target="#year_me">&nbsp; Computer Science and Engineering</a>
                        </li>
                        <li><a href="{{route('departments' , ['dept' => 'mech'])}}" data-toggle="collapse" data-target="#year_ce">&nbsp; Civil Engineering</a>
                          </li>
                        <li><a href="{{route('departments' , ['dept' => 'mech'])}}" data-toggle="collapse" data-target="#year_cst">&nbsp; Chemical Science and Technology</a>
                        </li>
                        <li><a href="{{route('departments' , ['dept' => 'mech'])}}" data-toggle="collapse" data-target="#year_c">&nbsp; Department of Chemistry</a>
                        </li>
                    </ul>
                </li>
                <li class="nav-header "> <a href="#" data-toggle="collapse" data-target="#menu2"> Courses </a>

                    <ul class="nav nav-stacked collapse" id="menu2">
                      @for($i = 0 ; $i < count($courses) ; $i++)
                      <li class="active"> <a href="{{route('course' , ['course_num' => $courses[$i]])}}">&nbsp; {{$courses[$i]}}</a></li>
                      @endfor
                    </ul>
                </li>

            </ul>
        </div>
        <div class="col-md-1">
        </div>
        <div class="col-sm-10">
                <h2><p class="text-info">Welcome to Student Information System IITG</p></h2>
                <br/>
                <h4>Recently Viewed Students : </h4>
              </div>
            <div class="col-sm-9">
        	<div class="row">

          @for($i = 0 ; $i < count($students) ; $i++)
        		 <div class="col-xs-15 col-sm-12 col-md-8 col-lg-8 col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-0.5 toppad" >


          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">{{$students[$i]->name}}</h3>
            </div>
            <div class="panel-body">
              <div class="row ">
                <div class="col-lg-3 " > <img alt="User Pic" src="{{URL::asset('images/User.png')}}" class="img-circle img-responsive"> </div>
                <div class="  col-lg-9 ">
                  <table class="table table-user-information">
                    <tbody>

                      <tr>
                        <td>Department:</td>
                        <td>{{$students[$i]->dept}}</td>
                      </tr>
                      <tr>
                        <td>Roll Number:</td>
                        <td>{{$students[$i]->roll_no}}</td>
                      </tr>
                      <tr>
                        <td>Year</td>
                        <td>{{$students[$i]->year}}</td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td><a href="mailto:{{$students[$i]->email}}">{{$students[$i]->email}}</a></td>
                      </tr>
                      <tr>
                        <td>Phone Number</td>
                        <td>{{$students[$i]->contact}}
                        </td>

                      </tr>

                    </tbody>
                  </table>


                  <a href="{{route('viewst' , ['roll_no' => $students[$i]->roll_no])}}" class="btn btn-primary">View More</a>&nbsp;&nbsp;&nbsp;
                  <a href="{{route('print' , ['roll_no' => $students[$i]->roll_no])}}" class="btn btn-primary">Generate PDF</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endfor
      </div>
    </div>
  </div>
</div>

</body>
    <script src="{{URL::asset('bootstrap/js/tests/vendor/jquery.min.js')}}"></script>
    <script src="{{URL::asset('bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script>
      function redirect(dept , year){
        window.location.href = onclick = "{{route('dashboard')}}" + '?dept='+dept+'&year='+year ;
      }
      $(document).ready(function(){$(".alert").addClass("in").fadeOut(4500);

        /* swap open/close side menu icons */
      $('[data-toggle=collapse]').click(function(){
        // toggle icon
        $(this).find("i").toggleClass("glyphicon-chevron-right glyphicon-chevron-down");
      });
    });
    </script>
</html>
