<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="*Test Commit*">

    <title>{{$c_name}}</title>

   <link rel="stylesheet" href="{{URL::asset('css/dashboard.css')}}">
<link rel="stylesheet" href="{{URL::asset('bootstrap/dist/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('bootstrap/dist/css/style.min.css')}}">

  <script type="text/javascript">
  window.onload = function () {
    var chart = new CanvasJS.Chart("chartContainer",
    {
      theme: "theme2",
      title:{
        text: "Grades Distribution"
      },
      animationEnabled: true,
      axisX: {

        interval:1,


      },
      axisY:{
        includeZero: false

      },
      data: [
      {
        type: "line",
        //lineThickness: 3,
        dataPoints: [
        { x: 4, y: {{$cpi[4]}} },
        { x: 5, y: {{$cpi[5]}} },
        { x: 6, y: {{$cpi[6]}} },
        { x: 7, y: {{$cpi[7]}} },
        { x: 8, y: {{$cpi[8]}} },
        { x: 9, y: {{$cpi[9]}} },
        { x: 10, y: {{$cpi[10]}} }


        ]
      }


      ]
    });

chart.render();
}
</script>
<script type="text/javascript" src="/assets/script/canvasjs.min.js"></script>
</head>
<body>
    <div id="wrapper">
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="navbar-header">

                <a class="navbar-brand" href="{{route('dashboard')}}">Student Information System</a>
      </div>
      <form method = "POST" action = {{route('search')}}>
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id = "token" >
      <div class="navbar-left">
          <div class="input-group custom-search-form">
              <input type="text" class="form-control" placeholder="Search..." name = "text" required>
              <span class="input-group-btn">
                <button class="btn btn-default"  type="submit" >
                  <i class= "	glyphicon glyphicon-search" ></i>
                </button>
              </span>

          </div>
      </div>
    </form>
      <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> {{$user->name}} <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><span class="glyphicon glyphicon-user"></span> Change Password</a>
                        </li>
                        <li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{ route('logout') }}"><span class="glyphicon glyphicon-off"></span> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
          </nav>
      <div class="row">
        <div class="col-sm-3">
            <!-- Left column -->
            <a href="{{route('dashboard')}}"><strong><i class="glyphicon glyphicon-home"></i> Dashboard</strong></a>

            <hr>

            <ul class="nav nav-stacked">
                <li class="nav-header "> <a href="#" data-toggle="collapse" data-target="#userMenu">Departments </a>
                    <ul class="nav nav-stacked collapse" id="userMenu">
                        <li> <a data-toggle="collapse" data-target="#year_cse" href="{{route('departments' , ['dept' => 'mech'])}}" >&nbsp; Mechanical Engineering &nbsp;</a>
                        </li>
                        <li><a href="{{route('departments' , ['dept' => 'mech'])}}" data-toggle="collapse" data-target="#year_ece">&nbsp; Electronics and Communication Engineering </a>
                        </li>
                        <li><a href="{{route('departments' , ['dept' => 'mech'])}}" data-toggle="collapse" data-target="#year_eee">&nbsp; Electrical Engineering</a>
                        </li>
                        <li><a href="{{route('departments' , ['dept' => 'mech'])}}" data-toggle="collapse" data-target="#year_mnc">&nbsp; Department of Mathematics</a>
                        </li>
                        <li><a href="{{route('departments' , ['dept' => 'mech'])}}" data-toggle="collapse" data-target="#year_me">&nbsp; Computer Science and Engineering</a>
                        </li>
                        <li><a href="{{route('departments' , ['dept' => 'mech'])}}" data-toggle="collapse" data-target="#year_ce">&nbsp; Civil Engineering</a>
                          </li>
                        <li><a href="{{route('departments' , ['dept' => 'mech'])}}" data-toggle="collapse" data-target="#year_cst">&nbsp; Chemical Science and Technology</a>
                        </li>
                        <li><a href="{{route('departments' , ['dept' => 'mech'])}}" data-toggle="collapse" data-target="#year_c">&nbsp; Department of Chemistry</a>
                        </li>
                    </ul>
                </li>
                <li class="nav-header "> <a href="#" data-toggle="collapse" data-target="#menu2"> Courses </a>

                      <ul class="nav nav-stacked collapse" id="menu2">
                        @for($i = 0 ; $i < count($courses) ; $i++)
                        <li> <a href="{{route('course' , ['course_num' => $courses[$i]])}}">&nbsp; {{$courses[$i]}}</a></li>
                        @endfor
                      </ul>
                </li>
            </ul>

        </div>

        <h2 align="center">Name of the course : {{$c_name}}</h2><br>
        <div class="col-sm-10">
          <div class="col-lg-12">
          <div class="box box-solid box-primary">
            <div class="box-header">
                <h3 class="box-title">Course Students</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <h3>Average grade : {{round($avg , 4)}}</h3>
              <table class="table table-striped sortable">
                <thread>
                  <tr>

                   <th bgcolor="#E6E6E6">Roll Number</th>
                    <th bgcolor="#E6E6E6">Student's Name</th>
                    <th bgcolor="#E6E6E6">Instructor</th>
                    <th bgcolor="#E6E6E6">Grade</th>
                    <th bgcolor="#E6E6E6">Year</th>
                  </tr>
              </thread>
                @for($i = 0 ; $i < count($data) ; $i++)

                  <tr>
                    <td><a href="{{route('viewst' , ['roll_no' => $data[$i]['roll_no']])}}">{{$data[$i]['roll_no']}}</a></td>
                    <td><a href="{{route('viewst' , ['roll_no' => $data[$i]['roll_no']])}}">{{$data[$i]['name']}}</a></td>
                    <td>{{$data[$i]['instructor']}}</td>
                    <td>{{$data[$i]['grade']}}</td>
                    <td>{{$data[$i]['year']}}</td>
                    <td>
                  </tr>
                @endfor
              </table>



            </div><!-- /.box-body -->
          </div><!-- /.box -->
          <div id="chartContainer" style="height: 300px; width: 100%;">
    </div>
        </div>
        </div>

      </div>
      <div id="chartContainer" style="height: 300px; width: 100%;">
  </div>
      </div>

      </body>


      <script src="{{URL::asset('bootstrap/js/tests/vendor/jquery.min.js')}}"></script>
      <script src="{{URL::asset('bootstrap/dist/js/bootstrap.min.js')}}"></script>
      <script src="{{URL::asset('js/sorttable.js')}}"></script>
      <script src="{{URL::asset('graphs/canvasjs.min.js')}}"></script>
      <script type="text/javascript">
 window.onload = function () {
   var chart = new CanvasJS.Chart("chartContainer",
   {
     theme: "theme2",
     title:{
       text: "Earthq"
     },
     animationEnabled: true,
     axisX: {

       interval:1


     },
     axisY:{
       includeZero: false

     },
     data: [
     {
       type: "line",
       //lineThickness: 3,
       dataPoints: [
         { x: 4, y: {{$cpi[4]}} },
         { x: 5, y: {{$cpi[5]}} },
         { x: 6, y: {{$cpi[6]}} },
         { x: 7, y: {{$cpi[7]}} },
         { x: 8, y: {{$cpi[8]}} },
         { x: 9, y: {{$cpi[9]}} },
         { x: 10, y: {{$cpi[10]}} }

       ]
     }


     ]
   });

chart.render();
}
</script>

      <script>

      $(document).ready(function(){$(".alert").addClass("in").fadeOut(4500);

      /* swap open/close side menu icons */
      $('[data-toggle=collapse]').click(function(){
      // toggle icon
      $(this).find("i").toggleClass("glyphicon-chevron-right glyphicon-chevron-down");
      });
      });
      </script>
      </html>
