<html>
<head>~
</head>
<body>
  <p style="text-align:center;" class="text-center"><h1 style="align:center;" >{{$student_personal->name}}'s Details</h1></p>
  <div class="col-sm-9">
  <div class="panel panel-default">
  <div class="panel-heading">
    <i class="fa fa-bell fa-fw"></i><h4 class="list-group-item-heading">Personal Info</h4>
  </div>
  <div class="panel-body">
    <div class="list-group">
        <p class="list-group-item-text"><b>Roll No. :</b> {{$student_personal->roll_no}}<br>
          <b>Name :</b> {{$student_personal->name}}<br>
          <b>Date of birth :</b> {{$student_personal->dob}}<br>
          <b>Department :</b> {{$student_personal->dept}}<br>
          <b>Year :</b> {{$student_personal->year}}<br>
          <b>Address :</b> {{$student_personal->address}}<br>
          <b>Contact :</b> {{$student_personal->contact}}<br>
          <b>Email :</b> {{$student_personal->email}}<br>
          <b>Father's name :</b> {{$student_personal->f_name}}<br>
          <b>Father's occupation :</b> {{$student_personal->f_occ}}<br>
          <b>Mother's occupation :</b> {{$student_personal->m_occ}}<br></p>

      </div>
    </div>
    <div class="panel-heading">
      <i class="fa fa-bell fa-fw"></i><h4 class="list-group-item-heading">Academic Information  </h4>
    </div>
    <div class="panel-body">
      <div class="list-group">

          <p class="list-group-item-text"><b>Tenth standard school :</b> {{$student_acad->tenth_school}}<br>
            <b>Tenth standard gpi :</b> {{$student_acad->tenth_gpi}}<br>
            <b>Inter college :</b> {{$student_acad->college}}<br>
            <b>Inter gpa :</b> {{$student_acad->inter_perc}}<br>
            <b>JEE Mains Rank :</b> {{$student_acad->mains_rank}}<br>
            <b>JEE Advanced Rank :</b> {{$student_acad->adv_rank}}<br>
            <b>Courses registered :</b> {{$student_acad->courses}}<br>
            <b>SPI :</b> {{$student_acad->spi_list}}<br>
            <b>CPI :</b> {{$student_acad->cpi}}<br>
            <b>Backlogs:</b> {{$student_acad->backlogs}}<br>
            <b>Scholarships received :</b> {{$student_acad->scholarships}}<br>
            <b>Faculty Advisor :</b> {{$student_acad->fac_adv}}<br>
            <b>Projects :</b> {{$student_acad->projects}}<br></p>

        </div>
      </div>
      <div class="panel-heading">
        <i class="fa fa-bell fa-fw"></i><h4 class="list-group-item-heading">Other Information</h4>
      </div>
      <div class="panel-body">
        <div class="list-group">

            <p class="list-group-item-text"><b>Clubs :</b> {{$student_other->clubs}}<br>
              <b>Medical Problems :</b> {{$student_other->med_prob}}<br></p>

          </div>
        </div>
      </div>
</body>
</html>
