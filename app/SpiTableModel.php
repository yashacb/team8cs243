<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpiTableModel extends Model
{
    //
    protected $table = 'spi_table' ;
    public $timestamps = false ;
}
