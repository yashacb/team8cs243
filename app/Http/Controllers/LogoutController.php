<?php

namespace App\Http\Controllers ;

use App\Http\Controllers\Controller ;
use Illuminate\Http\Request ;
use \Auth ;

class LogoutController extends Controller
{
  public function logoutUser(Request $request)
  {
    if(Auth::check())
    {
      Auth::logout() ;
    }
    return redirect('/sis')->withcookie(cookie('name' , '' , -1));
  }
}

?>
