<?php

namespace App\Http\Controllers ;

use App\Http\Controllers\Controller ;
use Illuminate\Http\Request;
use Illuminate\Http\Response ;
use \Auth ;
use App\StPersonalModel ;
use App\CoursesModel ;
use Illuminate\Support\Collection ;

class PostLoginController extends Controller
{
  public function showDashBoard(Request $request)
  {
    if(Auth::check())
    {
    $user = Auth::user() ;
      if($request->input('dept') != null && $request->input('year') != null)
      {
        $students = StPersonalModel::where('dept' , '=' , $request->input('dept'))->where('year' , '=' , intval($request->input('year')))->get() ;
        return view('dashboard' , ['user' => $user , 'students' => $students]) ;
      }
      $past_string = $user->past ;
      $roll_array = explode(',' , $past_string);
      $past_array = [] ;
      foreach($roll_array as $roll)
      {
        $past_array[] = StPersonalModel::where('roll_no' , '=' , intval($roll))->first() ;
      }
      $courses_list = [] ;
      $all_list = CoursesModel::all() ;
      foreach($all_list as $c)
        if(!in_array($c->course_num , $courses_list))
          $courses_list[] = $c->course_num ;
      sort($courses_list) ;
      return view('dashboard' , ['user' => $user , 'students' => $past_array , 'courses' => $courses_list]) ;
    }
    else
      return view('welcome' , ['error' => 'You need to login to view this information .']) ;
  }
}

 ?>
