<?php

namespace App\Http\Controllers ;

use App\Http\Controllers\Controller ;
use Illuminate\Http\Request ;
use Illuminate\Http\Response ;
use App\User ;
use App\StPersonalModel ;
use App\CoursesModel ;

class DashboardInfoController extends Controller
{
  public function studentInfo(Request $request , $dept = null , $year = null)
  {
    if(! \Auth::check())
      return (new Response('You have to login to view this information !')) ;
    else
    {
      try
      {
        $results = StPersonalModel::where(['dept' => $dept , 'year' => intval($year)]) -> get() ;
        $response_string = '<!DOCTYPE html><body><table>' ;
        if(count($results) != 0)
        {
          foreach($results as $student)
            $response_string = $response_string . '<tr><td>' . $student->name . '</td>' . '<td>' . $student->roll_no . '</td></tr>' ;
          $response_string .= '</table></body>' ;
          return (new Response($response_string)) ;
        }
        else
          return (new Response('Bad data entered .')) ;
      }
      catch(Exception $e)
      {
        return (new Response('Bad data entered .')) ;
      }
    }
  }
}

?>
