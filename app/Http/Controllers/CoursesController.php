<?php

namespace App\Http\Controllers ;

use App\Http\Controllers\Controller ;
use Illuminate\Http\Request ;
use Illuminate\Http\Response ;
use App\User ;
use App\CoursesModel ;
use App\StPersonalModel ;

class CourseController extends Controller
{
  public function show(Request $request , $course_num)
  {
    if(! \Auth::check())
      return view('welcome' , ['error' => 'You need to login to view this information .']) ;
    else
    {
      if(count(CoursesModel::where('course_num' , '=' , $course_num)->get()) === 0)
        return view('welcome' , ['error' => 'Bad data entered .']) ;
      $grades = [] ;
      $grades['AA'] = 10 ;
      $grades['AB'] = 9 ;
      $grades['BB'] = 8 ;
      $grades['BC'] = 7 ;
      $grades['CC'] = 6 ;
      $grades['CD'] = 5 ;
      $grades['DD'] = 4 ;
      $data = CoursesModel::where('course_num' , '=' , $course_num)->orderBy('grade' , 'ASC')->get() ;
      $avg = 0.0 ;
      $course_details = [] ;
      $i = 0 ;
      $cpi = [];
      for($x = 0; $x <= 10 ; $x++) {
             $cpi[$x] = 0;
     }
      foreach($data as $entry)
      {
        $avg += $grades[$entry->grade] ;
        $st_name = STPersonalModel::where('roll_no' , '=' , $entry->roll_no)->get()[0]->name ;
        $course_details[$i]['name'] = $st_name ;
        $course_details[$i]['roll_no'] = $entry->roll_no ;
        $course_details[$i]['year'] = $entry->year ;
        $course_details[$i]['instructor'] = $entry->instructor ;
        $course_details[$i]['grade'] = $entry->grade ;
        $course_details[$i]['year'] = $entry->year ;
        if($grades[$entry->grade] == 10)
        { $cpi[10]++;
        }
        if($grades[$entry->grade] == 9)
        { $cpi[9]++;}
        if($grades[$entry->grade] == 8)
        { $cpi[8]++;}
        if($grades[$entry->grade] == 7)
        { $cpi[7]++;}
        if($grades[$entry->grade] == 6)
        { $cpi[6]++;}
        if($grades[$entry->grade] == 5)
        { $cpi[5]++;}
        if($grades[$entry->grade] == 4 )
        { $cpi[4]++;}
        $i++ ;
      }
      $avg = $avg/count($course_details) ;
      // return new Response($course_details) ;
      $courses_list = [] ;
      $all_list = CoursesModel::all() ;
      foreach($all_list as $c)
        if(!in_array($c->course_num , $courses_list))
          $courses_list[] = $c->course_num ;
      sort($courses_list) ;
      return view('course' , ['avg' => $avg , 'c_name' => $data[0]->course_name , 'data' => $course_details , 'user' => \Auth::user() , 'courses' => $courses_list,'cpi' => $cpi]);
    }
  }
}

?>
