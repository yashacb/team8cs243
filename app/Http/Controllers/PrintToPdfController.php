<?php

namespace App\Http\Controllers ;

use App\Http\Controllers\Controller ;
use Illuminate\Http\Request;
use Illuminate\Http\Response ;
use \Auth ;
use App\StPersonalModel ;
use App\StAcadModel ;
use App\StOtherModel ;
use App\CoursesModel ;

class PrintToPdfController extends Controller
{
  public function printStudent(Request $request , $roll_no)
  {
    if(Auth::check())
    {
      if(is_numeric($roll_no) && (count(StPersonalModel::where('roll_no' , '=' , $roll_no)->get()) == 1))
      {
        $courses = CoursesModel::where('roll_no' , '=' , $roll_no)->get() ;
        $st_personal = StPersonalModel::where('roll_no' , '=' , $roll_no)->first() ;
        $st_acad = StAcadModel::where('roll_no' , '=' , $roll_no)->first() ;
        $st_other = StOtherModel::where('roll_no' , '=' , $roll_no)->first() ;
        $pdf = \PDF::loadView('PDF' , ['courses' => $courses , 'st_personal' => $st_personal , 'st_acad' => $st_acad , 'st_other' => $st_other]);
        return $pdf->download('student.pdf');
      }
      else
      {
        return view('welcome' , ['error' => 'Bad data entered .']) ;
      }
    }
    else
      return view('welcome' , ['error' => 'You need to login to view this information .']) ;
  }
}

 ?>
