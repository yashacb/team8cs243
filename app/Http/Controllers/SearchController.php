<?php

namespace App\Http\Controllers ;

use App\Http\Controllers\Controller ;
use Illuminate\Http\Request;
use Illuminate\Http\Response ;
use \Auth ;
use App\StPersonalModel ;
use App\StAcadModel ;
use App\StOtherModel ;
use App\CoursesModel ;
use DB;
use Input;

use Illuminate\Database\Eloquent\Model;


class SearchController extends Controller
{

    public function search(Request $request )
    {
      if(! \Auth::check())
        return view('welcome' , ['error' => 'You need to login to view this information .']);
      else
      {
          $user = Auth::user() ;
          $query = $request->input('text');
          if($query != null)
          {
            $array = (DB::table('st_personal')->where('name', 'LIKE', '%' . $query . '%')
            ->orwhere('roll_no', 'LIKE', '%' . $query . '%')
            ->orwhere('email', 'LIKE', '%' . $query . '%')
            ->orwhere('dept', 'LIKE', '%' . $query . '%')
            ->orwhere('contact', 'LIKE', '%' . $query . '%')
            ->get());
            $courses_list = [] ;
            $all_list = CoursesModel::all() ;
            foreach($all_list as $c)
              if(!in_array($c->course_num , $courses_list))
                $courses_list[] = $c->course_num ;
            sort($courses_list) ;
            return view('search_results',['text' => $query,'array'=>$array , 'user' =>$user , 'courses' => $courses_list] );
          }
          else
          {
            return view('welcome' , ['error' => 'Bad data entered']);
          }
      }
         }



    }

?>
