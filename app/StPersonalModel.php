<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StPersonalModel extends Model
{
    //
    protected $table = 'st_personal' ;
    public $timestamps = false ;
}
