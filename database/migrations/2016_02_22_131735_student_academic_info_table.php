<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StudentAcademicInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('st_acad' , function($table){
          $table->integer('roll_no');
          $table->primary('roll_no');
          $table->string('mains_rank');
          $table->string('adv_rank');
          $table->string('inter_perc');
          $table->string('college');
          $table->string('tenth_gpi');
          $table->string('tenth_school');
          $table->longText('courses');
          $table->string('spi_list');
          $table->string('cpi');
          $table->string('backlogs');
          $table->string('scholarships');
          $table->string('faculty_adv');
          $table->string('projects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('st_acad');
    }
}
