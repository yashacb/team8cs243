<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColumnInStPersonal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('st_personal', function (Blueprint $table) {
            //
            $table->integer('passing_year') ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('st_personal', function (Blueprint $table) {
            //
            $table->dropColumn('passing_year') ;
        });
    }
}
