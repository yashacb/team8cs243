<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spi_table', function (Blueprint $table) {
            $table->integer('roll_no');
            $table->foreign('roll_no')->references('roll_no')->on('st_personal')->onDelete('cascade') ;
            $table->float('spi') ;
            $table->tinyInteger('semester') ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('spi_table');
    }
}
