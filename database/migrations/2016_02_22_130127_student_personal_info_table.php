<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StudentPersonalInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('st_personal' , function($table){
          $table->integer('roll_no');
          $table->primary('roll_no');
          $table->string('name');
          $table->date('dob');
          $table->string('dept');
          $table->integer('year');
          $table->string('address');
          $table->string('contact');
          $table->string('email');
          $table->string('f_name');
          $table->string('f_occ');
          $table->string('m_name');
          $table->string('m_occ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('st_personal');
    }
}
